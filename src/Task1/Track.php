<?php

declare(strict_types=1);

namespace App\Task1;

class Track
{
    private float $lapLength;
    private int $lapsNumber;
    private array $track = [];

    public function __construct(float $lapLength, int $lapsNumber)
    {
        $this->lapLength = $this->validate($lapLength);
        $this->lapsNumber = $this->validate($lapsNumber);
    }
    private function validate($value)
    {
        if ($value < 0) {
            throw new \Exception('Negative value');
        }
        return $value;
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        $this->track[] = $car;
    }

    public function all(): array
    {
        return $this->track;
    }

    public function run(): Car
    {
        if (count($this->track) == 0) {
            throw new \Exception('Empty track');
        }
        $distance = $this->lapLength * 1000 * $this->lapsNumber;
        usort($this->track, fn($a, $b) => $a->raceResult($distance) <=> $b->raceResult($distance));
        return $this->track[0];
    }
}
