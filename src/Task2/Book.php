<?php

declare(strict_types=1);

namespace App\Task2;

class Book
{
    private string $title;
    private int $price;
    private int $pagesNumber;

    public function __construct(
        string $title,
        int $price,
        int $pagesNumber
    ) {
        $this->title = $title;
        $this->price = $this->validate($price);
        $this->pagesNumber = $this->validate($pagesNumber);
    }
    private function validate($value)
    {
        if ($value < 0) {
            throw new \Exception('Negative value');
        }
        return $value;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getPagesNumber(): int
    {
        return $this->pagesNumber;
    }
}
