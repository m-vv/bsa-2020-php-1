<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Car;
use App\Task1\Track;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
        $result = "<h1> Our participants </h1>".PHP_EOL;
        $result .= "<table>".PHP_EOL;
        $result .= "<thead>".PHP_EOL;
        $result .= "<tr>".PHP_EOL;
        $result .= "<th>Name, speed, Fuel Consumption</th> <th> Photo </th>".PHP_EOL;
        $result .= "</tr>".PHP_EOL;
        $result .= "<thead>".PHP_EOL;
        $result .= "</thead>".PHP_EOL;
        $result .= "<tbody>".PHP_EOL;
        foreach ($track->all() as $car) {
            $result .= "<tr>".PHP_EOL;
            $result .= "<td>{$car->getName()}: {$car->getSpeed()}, {$car->getFuelConsumption()} </td>"
                . "<td><img src=\"{$car->getImage()}\"></td>".PHP_EOL;
            $result .= "</tr>".PHP_EOL;
        }
        $result .= "</tbody>".PHP_EOL;
        $result .= "</table>".PHP_EOL;
        $result .= "<h1> Winner is</h1>".PHP_EOL;
        $winner = $track->run();
        $result .= "<h3> &#x1F3C6;&#x1F3C5;{$winner->getName()}&#x1F3C6;&#x1F3C5;</h3>" .PHP_EOL;
        return $result;
    }
}
